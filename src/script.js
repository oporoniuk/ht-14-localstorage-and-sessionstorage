/* Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?
// sessionStorage - зберігає дані лише під час поточної сесії, всі дані зникають, 
//якщо кладку закрити чи перезавантажити

// localStorage - зберігає дані допоки їх не видалять.
2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
// Сховище прив’язане до оригінального сайту (домен/протокол/порт). Таким чином, що різні протоколи
// або субдомени мають різні об’єкти зберігання, і не можуть отримати доступ до даних один одного.

3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
// видаляються

Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) 
// на ваш розсуд. При повторному натискання - повертати все як було спочатку - 
//начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/ 


const themeBtn= document.getElementById('themeBtn');
// const themeStyleSheet = document.getElementById('themeStyleSheet');

function changeTheme(){
    const selectedTheme = themeBtn.textContent;
    const link = document.getElementById('themeStyleSheet');
    if(themeBtn.textContent === 'Dark'){
        themeBtn.textContent = 'Light';
        link.href = `./src/css/style-light.css`;
    
    } else {
        themeBtn.textContent = 'Dark';
        link.href = `./src/css/style-dark.css`;
};

    localStorage.setItem('theme', selectedTheme); 
    console.log(`Theme changed to ${selectedTheme}`);
};

const savedTheme = localStorage.getItem('theme');
if (savedTheme) {
    themeBtn.textContent = savedTheme;
    changeTheme()
};

themeBtn.addEventListener('click', changeTheme)
console.log(savedTheme);


// themeBtn.addEventListener('click', function() {
//     if (themeBtn.textContent === 'Light Theme'){
//         themeStyleSheet.href = 'css/style-light.css'
//     } else {
//         themeBtn.textContent === 'Dark Theme';
//         themeStyleSheet.href = 'css/style-dark.css'
//     };
// });



// function changeTheme() {
//     const selectedTheme = themeBtn.value;
//     const link = getElementById("themeStyleSheet");
//     link.href = `styles-${selectedTheme}.css`;
//     localStorage.setItem('theme', selectedTheme);
// };
// import { themeSelect, changeTheme } from "./changeTheme"; 
// const savedTheme = localStorage.getItem('theme');
// if(savedTheme) {
//     themeSelect.value = savedTheme;
//     changeTheme();
// };

// themeSelect.addEventListener('change', changeTheme);